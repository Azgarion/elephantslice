﻿using System;

namespace ElephantSlice
{
    class Program
    {
        static void Main(string[] args)
        {
            int orderValue;
            
            Console.WriteLine("Enter how many item you want:");
            
            var numberOfItem = GetInputNumber();
            
            Console.WriteLine("Enter how much does this item cost :");
            
            var itemPrice = GetInputNumber();

            orderValue = itemPrice * numberOfItem;
            
            Console.WriteLine("Your order value is : " + orderValue);

            double? discountedOrderValue = null;
            
            if (orderValue > 1000)
            {
                double discountPercentage = GetDiscountPercentage(orderValue);
                
                Console.WriteLine("Your order give right to {0}% Discount", discountPercentage);

                discountedOrderValue = orderValue - CalculatePercentageValue(orderValue, discountPercentage);
                
                Console.WriteLine("Your order value after discount is : "+ discountedOrderValue);
            }
            
            Console.WriteLine("Country code available : ");

            Console.WriteLine(string.Join(", ", Enum.GetNames(typeof(CountryCode))));

            CountryCode countryCode = GetInputCountryCode();

            double taxPercentage = GetCountryTaxPercentage(countryCode);
            
            Console.WriteLine("The country with code "+countryCode+" apply a tax of {0}%.", taxPercentage );

            var value = discountedOrderValue ?? orderValue;
            var orderValueAfterTax = value + CalculatePercentageValue(value, taxPercentage);
            
            Console.WriteLine("Your order value after tax is : "+ orderValueAfterTax);
            Console.WriteLine("GoodBye.");
        }

        private static int GetInputNumber()
        {
            int number = 0;

            bool valid = false;
            while (!valid)
            {
                string input = Console.ReadLine();
                if (int.TryParse(input, out number))
                {
                    valid = true;
                }
                else
                {
                    Console.WriteLine("Not a number");
                }
            }

            return number;
        }

        private static CountryCode GetInputCountryCode()
        {
            bool country = false;

            CountryCode countryCode = CountryCode.UT;

            while (!country)
            {
                Console.WriteLine("Enter country code:");
                string input = Console.ReadLine();
                if (Enum.TryParse(input, out countryCode))
                {
                    country = true;
                }
                else
                {
                    Console.WriteLine("Country code doesn't supported");
                }
            }

            return countryCode;
        }

        private static double GetDiscountPercentage(int orderValue)
        {
            double discountedOrderValue = 0;
            
            if (orderValue >= 50000)
            {
                return 15;
            }
            
            if (orderValue >= 10000)
            {
                return 10;
            }
            
            if (orderValue >= 7000)
            {
                return 7;
            }
            
            if (orderValue >= 5000)
            {
                return 5;
            }
            
            if (orderValue >= 1000)
            {
                return 3;
            }

            return discountedOrderValue;
        }

        private static double GetCountryTaxPercentage(CountryCode countryCode)
        {
            switch (countryCode)
            {
                case CountryCode.UT:
                    return 6.85;
                    break;
                case CountryCode.NV:
                    return 8.00;
                    break;
                case CountryCode.TX:
                    return 6.25;
                    break;
                case CountryCode.AL:
                    return 4.00;
                    break;
                case CountryCode.CA:
                    return 8.25;
                    break;
                default:
                    Console.WriteLine("The country has no Tax.");
                    return 0;
                    break;   
            }
        }

        private static double CalculatePercentageValue(double value, double percentage)
        {
            return value / 100 * percentage;
        }
        
    }
}